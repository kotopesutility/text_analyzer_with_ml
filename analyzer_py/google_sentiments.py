from google.cloud import language_v1

def sample_analyze_sentiment(text):
    client = language_v1.LanguageServiceClient()

    type_ = language_v1.types.Document.Type.PLAIN_TEXT

    language = "en"
    document = {"content" : text, "type_" : type_, "language": language}

    encoding_type = language_v1.EncodingType.UTF8

    response = client.analyze_sentiment(request = {'document': document, 'encoding_type' : encoding_type})
    
    full_entity = 0
    full_magnetism = 0
    every_entity_score = []
    every_entity_magnetism = []

    full_entity = response.document_sentiment.score
    full_magnetism = response.document_sentiment.magnitude

    for sentence in response.sentences:
        every_entity_score.append(sentence.sentiment.score)
        every_entity_magnetism.append(sentence.sentiment.magnitude)

    return full_entity, full_magnetism, every_entity_score, every_entity_magnetism
