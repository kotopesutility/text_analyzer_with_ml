from nltk.classify import NaiveBayesClassifier
from nltk.corpus import subjectivity
from nltk.sentiment import SentimentAnalyzer
from nltk.sentiment.util import *
from nltk.sentiment.vader import SentimentIntensityAnalyzer
import nltk

def nltk_analyser(text):
    nltk.download('vader_lexicon')

    sid = SentimentIntensityAnalyzer()
    neg, neu, pos, compound = sid.polarity_scores(text).values()

    return pos, neu, neg
