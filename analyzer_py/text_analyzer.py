import json
from google_sentiments  import sample_analyze_sentiment as sas
from microsoft_azure_analyse import sentiment_analysis_example as sae
from analyse_nltk import nltk_analyser as na

def text_analyzer(file_w_t, mode, service):
    if mode == 'single':
        with open(file_w_t) as f:
            full_entity, full_magnetism, every_entity_score, every_entity_magnetism = sas('%s' % (f.read()))

        return full_entity, full_magnetism, every_entity_score, every_entity_magnetism

    else:
        fp = open(file_w_t)
        js = json.load(fp)
        fp.close()

        if service == 'google':
            full_e = []
            full_m = []
        elif service in ['micro', 'nltk']:
            full_pos = []
            full_neutral = []
            full_negative = []

        for x in js['messages']:
            if type(x['text']) == str:
                if service == 'google':
                    full_entity, full_magnetism, tresh, tresh_ = sas('%s' % (x['text']))
                    full_e.append(full_entity)
                    full_m.append(full_magnetism)
                elif service == 'micro':
                    plus, zero, minus = sae('%s' % (x['text']))
                    full_pos.append(plus)
                    full_neutral.append(zero)
                    full_negative.append(minus)
                elif service == 'nltk':
                    plus, zero, minus = na('%s' % (x['text']))
                    full_pos.append(plus)
                    full_neutral.append(zero)
                    full_negative.append(minus)
                    
        if service == 'google':
            return full_e, full_m, None, None
        elif service in ['micro', 'nltk']:
            return full_pos, full_neutral, full_negative
