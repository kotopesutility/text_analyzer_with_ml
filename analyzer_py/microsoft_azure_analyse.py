key = "a28ae463ce32428a9aeb75958a56db1c"
endpoint = "https://sentiment-ii-methods.cognitiveservices.azure.com/"

from azure.ai.textanalytics import TextAnalyticsClient
from azure.core.credentials import AzureKeyCredential


def authenticate_client():
    ta_credential = AzureKeyCredential(key)
    text_analytics_client = TextAnalyticsClient(
            endpoint=endpoint, 
            credential=ta_credential)
    return text_analytics_client


def sentiment_analysis_example(documents):
    client = authenticate_client()
    document = []
    document.append(documents)
    response = client.analyze_sentiment(documents=document)[0]
    r_val = response['sentences'][0]['confidence_scores'].values()
    plus = r_val[0]
    neutral = r_val[1]
    minus = r_val[2]
        
    return plus, neutral, minus
